TEMPLATE = subdirs

SUBDIRS += \
    CTester \
    INS3D \
    INSGL \
    INSIA \
    INSMA \
    INSMT \
    INSSH \
    INSUT


# where to find the sub projects - give the folders
INSMT.subdir   = INSMT
INSUT.subdir   = INSUT
INSSH.subdir   = INSSH
INSGL.subdir   = INSGL
INS3D.subdir   = INS3D
INSMA.subdir   = INSMA
CTester.subdir = CTester

#seta dependencias obrigatorias
INSUT.depends   = INSMT
INSSH.depends   = INSGL INSUT
INS3D.depends   = INSGL INSSH INSMA
INSGL.depends   = INSMT
INSMA.depends   = INSGL INSUT
CTester.depends = INS3D
