#include "IUT_AvlTree.h"

CIUTAvlTree::CIUTAvlTree()
{

}

//I need to redefine the insert and remove functions so the tree balances
void CIUTAvlTree::insert(CIUTreeNode value)
{
    m_insert (root,NULL,value);
    m_balance(root,NULL);
}

bool CIUTAvlTree::remove(CIUTreeNode &value)
{
    bool ret = m_remove(root,value);
    m_balance(root,NULL);
    return ret;
}

void CIUTAvlTree::m_balance(CIUTreeNode *&node, CIUTreeNode *parent)
{
    Q_UNUSED(node);
    Q_UNUSED(parent);
    //there could be multiple cases since you can insert an entire tree
    //therefore we must check the balance of the entire tree
}
