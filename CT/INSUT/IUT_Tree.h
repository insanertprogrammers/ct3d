/**********************************************************************************/
/*Projeto             :Insane RT Framework                                        */
/*Descricao           :                                                           */
/*Data de Criação     :                                                           */
/*                                                                                */
/*Copyright (c) 2013 William Wolff. Todos os direitos reservados                  */
/**********************************************************************************/

#ifndef IUT_TREE_H
#define IUT_TREE_H

#include "IUT_GlobalDef.h"
#include "IUT_TreeNode.h"

class CIUTree
{
public:
     CIUTree();

    ~CIUTree();

	//We will define these all as virtuals for inherited trees (like a Huffman Tree and AVL Tree shown later)
    virtual void          insert(CIUTreeNode &value);
    virtual CIUTreeNode*  search(CIUTreeNode &value);
    virtual bool          remove(CIUTreeNode &value);

    virtual bool operator< (CIUTree &other);

     void operator= (CIUTree &other);

    CIUTreeNode*& first();

protected:

    void m_equal  (CIUTreeNode*& node, CIUTreeNode*     value                         );
    void m_destroy(CIUTreeNode * node                                                 );
    void m_insert (CIUTreeNode *&node, CIUTreeNode     *parent, CIUTreeNode     &value);
    void m_insert (CIUTreeNode *&node, CIUTreeNode     *parent, CIUTree &tree         );

    CIUTreeNode* m_search(CIUTreeNode *node, CIUTreeNode &value);

    bool m_remove(CIUTreeNode *node, CIUTreeNode &value);

    //this will be our root node and private functions
    CIUTreeNode *root;
};

#endif // IUT_TREE_H
