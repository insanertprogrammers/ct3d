#-------------------------------------------------
#
# Project created by QtCreator 2014-08-30T18:08:59
#
#-------------------------------------------------

QT       += core

TARGET = INSUT
TEMPLATE = lib
CONFIG += staticlib

SOURCES += INSUT.cpp \
    IUT_AvlTree.cpp \
    IUT_CarregadorArquivos.cpp \
    IUT_GenericData.cpp \
    IUT_GerenciadorPath.cpp \
    IUT_HuffmanTree.cpp \
    IUT_Tree.cpp \
    IUT_TreeNode.cpp \
    IUT_Trigger.cpp \
    IUT_ParametroTrigger.cpp \
    IUT_Node.cpp \
    IUT_Lista.cpp \
    IUT_FilaPrioridade.cpp

HEADERS += INSUT.h \
    IUT_AvlTree.h \
    IUT_GenericData.h \
    IUT_GlobalDef.h \
    IUT_Grafo.h \
    IUT_HuffmanTree.h \
    IUT_Lista.h \
    IUT_Tree.h \
    IUT_CarregadorArquivos.h \
    IUT_GerenciadorPath.h \
    IUT_FilaPrioridade.h \
    IUT_TreeNode.h \
    IUT_Trigger.h \
    IUT_ParametroTrigger.h \
    IUT_Node.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

#INSMT
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSMT/release/ -lINSMT
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSMT/debug/ -lINSMT
else:unix: LIBS += -L$$OUT_PWD/../INSMT/ -lINSMT

INCLUDEPATH += $$PWD/../INSMT
DEPENDPATH += $$PWD/../INSMT

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/release/libINSMT.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/debug/libINSMT.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/release/INSMT.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/debug/INSMT.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../INSMT/libINSMT.a


#assimp
win32: LIBS += -L$$PWD/../libs/assimp/MinGW/lib/ -llibassimp.dll

INCLUDEPATH += $$PWD/../libs/assimp/MinGW/include
DEPENDPATH += $$PWD/../libs/assimp/MinGW/include

