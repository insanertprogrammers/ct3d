#include "IUT_TreeNode.h"

CIUTreeNode::CIUTreeNode()
{
    left = right = parent = NULL;
}

/*
CIUTreeNode::CIUTreeNode(CIUTGenericData<int> &value)
{
    data = &value;
}
*/

CIUTreeNode::~CIUTreeNode()
{

}

void CIUTreeNode::operator= (const CIUTreeNode &other)
{
    data = other.data;
}

bool CIUTreeNode::operator< (CIUTreeNode &other)
{
    return (data < other.data);
}
