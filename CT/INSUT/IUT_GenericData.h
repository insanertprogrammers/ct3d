#ifndef CIUTGENERICDATA_H
#define CIUTGENERICDATA_H

#include "IUT_GlobalDef.h"

class CIUTGenericData
{
public:

    CIUTGenericData(                     );
    CIUTGenericData(CIUTGenericData &copy);
   ~CIUTGenericData(                     );

    bool    setData(void *pData);

    QString getTypeName();
    void*   getData();
    int     getType();

    int     IUT_DataType;
    void*   IUT_Data;
};

#endif // CIUTGENERICDATA_H
