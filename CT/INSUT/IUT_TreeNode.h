#ifndef CIUTREENODE_H
#define CIUTREENODE_H

#include "IUT_GlobalDef.h"
#include "IUT_GenericData.h"

//lets implement a basic templated tree
class CIUTreeNode
{
public:
    void *data;
    CIUTreeNode     *left, *right, *parent;

    CIUTreeNode();
    //CIUTreeNode(CIUTGenericData<int> &value);
   ~CIUTreeNode();

    void operator= (const CIUTreeNode &other);
    bool operator< (CIUTreeNode       &other);

};

#endif // CIUTREENODE_H
