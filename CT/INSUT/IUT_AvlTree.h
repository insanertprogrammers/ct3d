#ifndef CIUTAVLTREE_H
#define CIUTAVLTREE_H

#include "IUT_GlobalDef.h"
#include "IUT_Tree.h"

class CIUTAvlTree : public CIUTree
{
public:

    CIUTAvlTree();

    //I need to redefine the insert and remove functions so the tree balances
    void insert(CIUTreeNode  value);
    bool remove(CIUTreeNode &value);


private:
    void m_balance(CIUTreeNode *&node, CIUTreeNode *parent);
};


#endif // CIUTAVLTREE_H
