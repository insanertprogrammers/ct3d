#ifndef CIUTHUFFMANTREE_H
#define CIUTHUFFMANTREE_H

#include "IUT_GlobalDef.h"
#include "IUT_Tree.h"

class CIUTHuffmanTree : public CIUTree
{
public:

    CIUTHuffmanTree();
    //I have to put this again otherwise the 2nd insert function will overwrite the virtual void
    void insert      (CIUTreeNode     &value);
    void insert      (CIUTHuffmanTree &tree);
    void insert_left (CIUTree &tree);
    void insert_right(CIUTree &tree);
};


#endif // CIUTHUFFMANTREE_H
