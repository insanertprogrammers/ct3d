/**********************************************************************************/
/*Projeto             :Insane RT Framework                                        */
/*Descricao           :                                                           */
/*Data de Criação     :                                                           */
/*                                                                                */
/*Copyright (c) 2013 William Wolff. Todos os direitos reservados                  */
/**********************************************************************************/

#include "IUT_Tree.h"

CIUTree::CIUTree()
{
    root = NULL;
}

CIUTree::~CIUTree()
{
    m_destroy(root);
}

//We will define these all as virtuals for inherited trees (like a Huffman Tree and AVL Tree shown later)
void CIUTree::insert(CIUTreeNode &value)
{
    m_insert(root,NULL,value);
}

CIUTreeNode* CIUTree::search(CIUTreeNode &value)
{
    return m_search(root,value);
}

bool CIUTree::remove(CIUTreeNode &value)
{
    return m_remove(root,value);
}

bool CIUTree::operator< (CIUTree &other)
{
    return (root->data < other.first()->data);
}

void CIUTree::operator= (CIUTree &other)
{
    m_equal(root,other.first());
}

CIUTreeNode*& CIUTree::first()
{
    return root;
}

void CIUTree::m_equal(CIUTreeNode*& node, CIUTreeNode* value)
{
    if(value != NULL)
    {
        node = new CIUTreeNode();
        *node = *value;
        if(value->left != NULL)
            m_equal(node->left, value->left);
        if(value->right != NULL)
            m_equal(node->right, value->right);
    }
}

void CIUTree::m_destroy(CIUTreeNode* value)
{
    if(value != NULL)
    {
        m_destroy(value->left);
        m_destroy(value->right);
        delete value;
    }
}

void CIUTree::m_insert(CIUTreeNode *&node, CIUTreeNode *parent, CIUTreeNode &value)
{
    if(node == NULL)
    {
        node = new CIUTreeNode();
        *node = value;
        node->parent = parent;
    }
    else if(value.data < node->data)
    {
        m_insert(node->left,node,value);
    }
    else
        m_insert(node->right,node,value);
}

void CIUTree::m_insert(CIUTreeNode *&node, CIUTreeNode *parent, CIUTree &tree)
{
    CIUTreeNode *value = tree.first();

    if(node == NULL)
    {
        node = new CIUTreeNode();
        *node = *value;
        node->parent = parent;
    }
    else if(value->data < node->data)
    {
        m_insert(node->left,node,tree);
    }
    else
        m_insert(node->right,node,tree);
}

CIUTreeNode* CIUTree::m_search(CIUTreeNode *node, CIUTreeNode &value)
{
    if(node == NULL)
        return NULL;
    else if(value.data == node->data)
        return node;
    else if(value.data < node->data)
        return m_search(node->left,value);
    else
        return m_search(node->right,value);
}

bool CIUTree::m_remove(CIUTreeNode *node, CIUTreeNode &value)
{
    //Just to avoid warning
    Q_UNUSED(node);

    //messy, need to speed this up later
    CIUTreeNode *tmp = m_search(root,value);

    if(tmp == NULL)
        return false;

    CIUTreeNode *parent = tmp->parent;

    //am i the left or right of the parent?
    bool iamleft = false;
    if(parent->left == tmp)
        iamleft = true;

    if(tmp->left != NULL && tmp->right != NULL)
    {
        if(parent->left == NULL || parent->right == NULL)
        {
            parent->left = tmp->left;
            parent->right = tmp->right;
        }
        else
        {
            if(iamleft)
                parent->left = tmp->left;
            else
                parent->right = tmp->left;

            CIUTreeNode* data = tmp->right;

            delete tmp;

            m_insert(root,NULL,*data);
        }
    }
    else if(tmp->left != NULL)
    {
        if(iamleft)
            parent->left = tmp->left;
        else
            parent->right = tmp->left;
    }
    else if(tmp->right != NULL )
    {
        if(iamleft)
            parent->left = tmp->right;
        else
            parent->right = tmp->right;
    }
    else
    {
        if(iamleft)
            parent->left = NULL;
        else
            parent->right = NULL;
    }
    return true;
}
