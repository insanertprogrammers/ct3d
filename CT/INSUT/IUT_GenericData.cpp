/**********************************************************************************/
/*Projeto             :Insane RT Framework                                        */
/*Descricao           :                                                           */
/*Data de Criação     :                                                           */
/*                                                                                */
/*Copyright (c) 2013 William Wolff. Todos os direitos reservados                  */
/**********************************************************************************/

#include "IUT_GenericData.h"

CIUTGenericData::CIUTGenericData()
{
    IUT_DataType = IUT_DATA_TYPE_MODEL;
    IUT_Data     = NULL;
}

CIUTGenericData::CIUTGenericData(CIUTGenericData &copy)
{
   string s = typeid(copy).name();
   if(typeid(IUT_Data)==typeid(copy))
   {
       IUT_DataType = copy.IUT_DataType;
       IUT_Data     = new CIUTGenericData(copy);
   }
}

bool CIUTGenericData::setData(void *pData)
{
    IUT_Data = pData;

    string type =typeid(IUT_Data).name();

    int nfold;

    if((nfold = (int)type.find("CTStaticModel"      ))!= -1)IUT_DataType = IUT_DATA_TYPE_MODEL;
    if((nfold = (int)type.find("CTTerrain"          ))!= -1)IUT_DataType = IUT_DATA_TYPE_TERRAIN;
    if((nfold = (int)type.find("CTSkyBox"           ))!= -1)IUT_DataType = IUT_DATA_TYPE_SKY;
    if((nfold = (int)type.find("CTHierarquicalModel"))!= -1)IUT_DataType = IUT_DATA_TYPE_HMODEL;

    return true;
}

CIUTGenericData::~CIUTGenericData()
{

}

QString CIUTGenericData::getTypeName()
{
   int *status = NULL;
   return abi::__cxa_demangle(typeid(IUT_Data).name(),NULL,NULL,status);
}

void* CIUTGenericData::getData()
{
   return IUT_Data;
}

int CIUTGenericData::getType()
{
   return IUT_DataType;
}
