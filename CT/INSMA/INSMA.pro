#-------------------------------------------------
#
# Project created by QtCreator 2016-01-03T19:48:20
#
#-------------------------------------------------

QT       += core opengl multimedia

TARGET = INSMA
TEMPLATE = lib
CONFIG += staticlib

SOURCES += INSMA.cpp \
    IMA_Material.cpp \
    IMA_TextureManager.cpp \
    IMA_Textura.cpp \
    IMA_MaterialManager.cpp \
    IMA_Cor.cpp \
    IMA_ColorMap.cpp \
    IMA_DepthMap.cpp

HEADERS += INSMA.h \
    IMA_GlobalDef.h \
    IMA_Material.h \
    IMA_TextureManager.h \
    IMA_Textura.h \
    IMA_MaterialManager.h \
    IMA_Cor.h \
    IMA_ColorMap.h \
    IMA_DepthMap.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}


#INSGL
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSGL/release/ -lINSGL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSGL/debug/ -lINSGL
else:unix: LIBS += -L$$OUT_PWD/../INSGL/ -lINSGL

INCLUDEPATH += $$PWD/../INSGL
DEPENDPATH += $$PWD/../INSGL

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/release/libINSGL.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/debug/libINSGL.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/release/INSGL.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/debug/INSGL.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../INSGL/libINSGL.a

#OpenCV
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_calib3d410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_core410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_dnn410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_features2d410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_flann410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_gapi410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_highgui410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_imgcodecs410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_imgproc410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_ml410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_objdetect410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_photo410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_stitching410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_video410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_videoio410.dll

INCLUDEPATH += $$PWD/../libs/opencv/MinGW/cv73064/include
DEPENDPATH += $$PWD/../libs/opencv/MinGW/cv73064/include
