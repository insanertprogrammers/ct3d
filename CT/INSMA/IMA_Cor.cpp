/**********************************************************************************/
/*Projeto             :Insane RT Framework                                        */
/*Descricao           :                                                           */
/*Data de Criação     :                                                           */
/*                                                                                */
/*Copyright (c) 2013 William Wolff. Todos os direitos reservados                  */
/**********************************************************************************/
#include "IMA_GlobalDef.h"
#include "IMA_Cor.h"


CIMACor::CIMACor()
{
    cor[0]  =0.0f;
    cor[1]  =0.0f;
    cor[2]  =0.0f;
    cor[3]  =1.0f;
}

CIMACor::CIMACor(float i3dCorR ,
                 float i3dCorG ,
                 float i3dCorB ,
                 float i3dCorA )
{
    cor[0]  = i3dCorR;
    cor[1]  = i3dCorG;
    cor[2]  = i3dCorB;
    cor[3]  = i3dCorA;
}



void CIMACor::SetaCor(float i3dCorR ,
                      float i3dCorG ,
                      float i3dCorB ,
                      float i3dCorA )
{
    cor[0]  = i3dCorR;
    cor[1]  = i3dCorG;
    cor[2]  = i3dCorB;
    cor[3]  = i3dCorA;
}

CIMACor CIMACor::PegaCor()
{
    return *this;
}

float * CIMACor::PegaCorRGBA()
{
    return cor;
}

float CIMACor::PegaCorR()
{
    return cor[0];
}


float CIMACor::PegaCorG()
{
    return cor[1];
}


float CIMACor::PegaCorB()
{
    return cor[2];
}


float CIMACor::PegaCorA()
{
    return cor[3];
}

