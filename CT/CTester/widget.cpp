#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    pGLWidget = new GLWidget(this);

    resize(600,600);

    int desktopArea = QApplication::desktop()->width() *
                     QApplication::desktop()->height();

    int widgetArea = width() * height();

    pGLWidget->resize(width(),height());

    if (((float)widgetArea / (float)desktopArea) < 0.75f)
    {
        show();
    }
    else
    {
        showMaximized();
    }
}

Widget::~Widget()
{
}

void Widget::keyPressEvent(QKeyEvent *event)
{
   pGLWidget->keyPressEvent(event);
}

void Widget::resizeEvent(QResizeEvent *event)
{
   pGLWidget->resize(event->size());
}
