QT       += core gui opengl openglextensions multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#Define copia do executavel no diretorio /bin
DESTDIR = $$PWD/../bin

SOURCES += \
    glwidget.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    glwidget.h \
    widget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#INS3D
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INS3D/release/ -lINS3D
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INS3D/debug/ -lINS3D

INCLUDEPATH += $$PWD/../INS3D
DEPENDPATH += $$PWD/../INS3D

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INS3D/release/libINS3D.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INS3D/debug/libINS3D.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INS3D/release/INS3D.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INS3D/debug/INS3D.lib

#INSMA
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSMA/release/ -lINSMA
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSMA/debug/ -lINSMA

INCLUDEPATH += $$PWD/../INSMA
DEPENDPATH += $$PWD/../INSMA

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/release/libINSMA.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/debug/libINSMA.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/release/INSMA.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/debug/INSMA.lib

#OpenCV
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_calib3d410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_core410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_dnn410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_features2d410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_flann410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_gapi410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_highgui410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_imgcodecs410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_imgproc410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_ml410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_objdetect410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_photo410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_stitching410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_video410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_videoio410.dll

INCLUDEPATH += $$PWD/../libs/opencv/MinGW/cv73064/include
DEPENDPATH += $$PWD/../libs/opencv/MinGW/cv73064/include

#INSGL
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSGL/release/ -lINSGL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSGL/debug/ -lINSGL

INCLUDEPATH += $$PWD/../INSGL
DEPENDPATH += $$PWD/../INSGL

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/release/libINSGL.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/debug/libINSGL.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/release/INSGL.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/debug/INSGL.lib

#INSSH
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSSH/release/ -lINSSH
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSSH/debug/ -lINSSH

INCLUDEPATH += $$PWD/../INSSH
DEPENDPATH += $$PWD/../INSSH

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/release/libINSSH.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/debug/libINSSH.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/release/INSSH.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/debug/INSSH.lib

#INSMT
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSMT/release/ -lINSMT
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSMT/debug/ -lINSMT

INCLUDEPATH += $$PWD/../INSMT
DEPENDPATH += $$PWD/../INSMT

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/release/libINSMT.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/debug/libINSMT.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/release/INSMT.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/debug/INSMT.lib

#INSUT
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSUT/release/ -lINSUT
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSUT/debug/ -lINSUT

INCLUDEPATH += $$PWD/../INSUT
DEPENDPATH += $$PWD/../INSUT

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/release/libINSUT.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/debug/libINSUT.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/release/INSUT.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/debug/INSUT.lib

#assimp
win32: LIBS += -L$$PWD/../libs/assimp/MinGW/lib/ -llibassimp.dll

INCLUDEPATH += $$PWD/../libs/assimp/MinGW/include
DEPENDPATH += $$PWD/../libs/assimp/MinGW/include

#INSIA
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSIA/release/ -lINSIA
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSIA/debug/ -lINSIA

INCLUDEPATH += $$PWD/../INSIA
DEPENDPATH += $$PWD/../INSIA

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSIA/release/libINSIA.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSIA/debug/libINSIA.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSIA/release/INSIA.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSIA/debug/INSIA.lib
