#include "glwidget.h"

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
   japosicionou          =false;
}

void GLWidget::cleanup()
{
    makeCurrent();
    doneCurrent();
}

void GLWidget::initializeGL()
{
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);

    //Conecta o Timer com a Função update para simular o loop infinito...
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(0);

    //Inicializa O contador de tempo percorrido
    elapsedtimer = new QElapsedTimer();
    elapsedtimer->start();

    //Com o contexto de Renderização, inicializamos o componente 3D
    pI3DComponent = new INS3D(context());

    //Inicializamos todas as funções OpenGL
    initializeOpenGLFunctions();

    //Inicializamos aqui por facilidade.... :)
    /***********************************************/
    /*Cria a Camera                                */
    /***********************************************/
    pCam = new CI3DCamera          ;//Instancia uma nova camera
    pCam   ->Inicializa()          ;//Inicializa a camera com valores default
    pCam   ->I3D_Velocidade = 25.0f;//seta a velocidade da camera

    /***********************************************/
    /*Cria os Objetos de Teste                     */
    /***********************************************/
    pSkyBox    = new CTSkyBox();
    pTerrain   = new CTTerrain();
    pModel     = new CTStaticModel();
    pTorre     = new CTFlagModel();
    pLight     = new CTLight();
    pCharacter = new CTHierarquicalModel();
    pLocator   = new CTPathfind();

    static_cast<CTPathfind*>(pLocator)->SetTerrain(static_cast<CTTerrain*>(pTerrain));

    //pObjectPool.push_back(pSkyBox);
    //pObjectPool.push_back(pTerrain);
    //pObjectPool.push_back(pModel);
    //pObjectPool.push_back(pTorre);
    //pObjectPool.push_back(pLight);
    //pObjectPool.push_back(pCharacter);
    //pObjectPool.push_back(pLocator);

    CIUTGenericData *tGenSky       = new CIUTGenericData();
    tGenSky->setData(pSkyBox);

    CIUTGenericData *tGenTerrain   = new CIUTGenericData();
    tGenTerrain->setData(pSkyBox);

    CIUTGenericData *tGenModel     = new CIUTGenericData();
    tGenModel->setData(pSkyBox);

    CIUTGenericData *tGenTorre     = new CIUTGenericData();
    tGenTorre->setData(pSkyBox);

    CIUTGenericData *tGenLight     = new CIUTGenericData();
    tGenLight->setData(pSkyBox);

    CIUTGenericData *tGenCharacter = new CIUTGenericData();
    tGenCharacter->setData(pSkyBox);

    CIUTGenericData *tGenLocator   = new CIUTGenericData();
    tGenLocator->setData(pSkyBox);


    pGenericPool.push_back(tGenSky);
    pGenericPool.push_back(tGenSky);
    pGenericPool.push_back(tGenTerrain);
    pGenericPool.push_back(tGenTorre);
    pGenericPool.push_back(tGenLight);
    pGenericPool.push_back(tGenCharacter);
    pGenericPool.push_back(tGenLocator);


    /***********************************************/
    /*Cria o Cubo no centro do Piso                */
    /***********************************************/
    /*****************************************************************/
    /*Cria um novo cubo na posição (0,0,0) com dimensão (10,10,10) no*/
    /*sentido anti horário                                           */
    /*****************************************************************/
    pCubo           = new CI3DCubo  (static_cast<CTTerrain*>(pTerrain)->PegaPosTrr(32,32) ,
                                     CIMTVetor(10.0f,10.0f,10.0f),
                                     IGL_WINMODE_CCW             );
    /***********************************************/
    /*Adiciona os materiais do Cubo                */
    /***********************************************/
    int IndiceMaterial;

    IndiceMaterial=INSMA::pINSMAMaterialManager->AddMaterial("Material Caixa",IGL_MATERIAL_TEXTURE);

    INSMA::pINSMAMaterialManager->AddTextura(IndiceMaterial,
                                             "world/Texturas/Caixa.bmp",
                                             IGL_TEXTURE_LINEAR);

    INSMA::pINSMAMaterialManager->SetaRejeicaoCor(IndiceMaterial,false,CIMACor(0.0f,0.0f,0.0f,1.0f));

    /*****************************************************************/
    /*Adiciona material ao Cubo                                      */
    /*****************************************************************/
    pCubo->I3D_Mesh->SetaMaterial(IndiceMaterial);

    /*****************************************************************/
    /*Posiciona o Carro e o CheckPoint                               */
    /*****************************************************************/
    pTorre->Translate(static_cast<CTTerrain*>(pTerrain)->PegaPosTrr(200,200),false);
    pCharacter->Translate(static_cast<CTTerrain*>(pTerrain)->PegaPosTrr(100,100),false);

}

//Esta Funcao foi colocada aqui apenas para demonstrar que nao é a todo
//ciclo que a tela é desenhada, portanto dificultoso de simular a alteração
//da cena no tempo real(Que está sendo simulada via connect()
void GLWidget::paintGL()
{
    TimeElapsed  = elapsedtimer->restart();
    TimeElapsed /= 1000;

    /*******************************************************************/
    /*Configura os Estados Iniciais da Maquina de Estado da OpenGL     */
    /*O Loop Principal de Renderização de Objetos deve ser colocado    */
    /*Entre o Inicio e o Fim deste Bloco                               */
    /*******************************************************************/
    pI3DComponent->EnableStates();
    pI3DComponent->Enable(INSSH::pISHProgramManager->ProgramId("CoreRender"),IGL_WINMODE_CW);

    /********************************************************************/
    /*Atualiza a Camera (O Posicionamento precisa ficar dentro do Bloco)*/
    /********************************************************************/
    pCam->PosicionaCamera();

    /********************************************************************/
    /*O Primeiro Objeto a Renderizar sempre é a SkyBox...FarDistance    */
    /********************************************************************/
    pSkyBox->Translate(CIMTVetor( 0.0f, 0.0f, 0.0f),false);
    pSkyBox->Render();

    /********************************************************************/
    /*Depois O Chão pois tudo irá em cima dele, certo?                  */
    /********************************************************************/
    pTerrain->Render();

    /********************************************************************/
    /*Renderiza a Caixa                                                 */
    /********************************************************************/
    pCubo->Render(IGL_MODE_RENDER     ,
                  IGL_RENDER_TRIANGLES,
                  IGL_SHADING_MODE_FLAT);

    /********************************************************************/
    /*Renderiza modelo dinâmico                                         */
    /********************************************************************/
    pI3DComponent->Enable(INSSH::pISHProgramManager->ProgramId("CoreRender"),IGL_WINMODE_CCW);
    pModel->Render();
    pCharacter->Render();
    pTorre->Render();
    pI3DComponent->Enable(INSSH::pISHProgramManager->ProgramId("CoreRender"),IGL_WINMODE_CW);

    /********************************************************/
    /*Atualiza Todos os Controles do Cenario                */
    /*Basicamente Durante a Renderização são estabelacidos  */
    /*objetos que controlam o comportamento geral do cenario*/
    /*como luzes ambiente que afetam as cores dos materiais */
    /*de cada objeto na cena, bem como a forma de shading   */
    /*Este metodo é responsavel em atualizar estes controles*/
    /********************************************************/
    if(!japosicionou)
    {
       SetInitPosition(pCam,pCubo->I3D_Pos);
       japosicionou = true;
    }
    else
    {
        UpdateScene();
    }

    /*******************************************************************/
    /*Desativa  os Estados Iniciais da Maquina de Estado da OpenGL     */
    /*O Loop Principal de Renderização de Objetos deve ser colocado    */
    /*Entre o Inicio e o Fim deste Bloco                               */
    /*******************************************************************/
    pI3DComponent->DisableStates();

}

void GLWidget::SetInitPosition(CI3DCamera *pCam, CIMTVetor centro)
{
    CIMTVetor posicao;

    /***********************************/
    /*Seta a Posicao Alvo              */
    /***********************************/
    posicao    = CIMTVetor(centro);
    posicao.z += 30.0f;
    /***********************************/
    /*Altera Posicao e View da Camera  */
    /***********************************/
    pCam->I3D_CameraPos  = posicao;
    pCam->I3D_CameraView = centro;

    /*****************************************/
    /*Atribui posição inicial da luz ambiente*/
    /*****************************************/
    pLight->Translate(static_cast<CTTerrain*>(pTerrain)->PegaCentro(),false);

    japosicionou = true;
}

void GLWidget::UpdateScene()
{
    /******************************************/
    /*Movimenta as nuvens no céu              */
    /******************************************/
    pSkyBox->Rotate(CIMTVetor(0.0f,0.05f,0.0f),CIMTVetor(0.0f,0.0f,0.0f));

    pModel->Translate(static_cast<CTTerrain*>(pTerrain)->PegaPosTrr(127,127),false);
    static_cast<CTLight*>(pLight)->Animate(static_cast<CTTerrain*>(pTerrain)->PegaCentro());

    CIMTVetor CharacterPos;
    CIMTVetor CheckpointPos;
    switch(static_cast<CTHierarquicalModel*>(pCharacter)->status)
    {
    case 0:
    {
        static_cast<CTHierarquicalModel*>(pCharacter)->GetPos(CharacterPos);

        CheckpointPos = static_cast<CTFlagModel*>(pTorre)->Pos();

        static_cast<CTPathfind*>(pLocator)->SetIni(CharacterPos.x ,CharacterPos .z);
        static_cast<CTPathfind*>(pLocator)->SetFim(CheckpointPos.x,CheckpointPos.z);

        static_cast<CTPathfind*>(pLocator)->FindPath();

        static_cast<CTHierarquicalModel*>(pCharacter)->status = 1;
    }break;
    case 1:
    {
        CIMTVetor NextStep;
        if(static_cast<CTPathfind*>(pLocator)->GetNextStep(NextStep))
        {
            static_cast<CTHierarquicalModel*>(pCharacter)->Translate(NextStep,false);
            //pCharacter->Rotate(CIMTVetor(0.0f,0.5f,0.0f),pTerrain->PegaCentro());
            //pCharacter->Scale(CIMTVetor(5.5f,5.5f,5.5f),pTerrain->PegaCentro());
        }
    }break;
    }

    pTorre->Translate(static_cast<CTTerrain*>(pTerrain)->PegaPosTrr(200,200),false);

    return;
}

void GLWidget::resizeGL(int w, int h)
{
    /*****************************/
    /*Calcula Frustrum de Camera */
    /*****************************/
    pCam->AtTela=w;
    pCam->LgTela=h;
    pCam->CalculaPerspectiva(1.0f,30000.0f,IGL_PROJECTION_PERSP);

}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    QPoint point,centro;
    point = event->pos();
    //centro.rx() = width() / 2;
    //centro.ry() = height()/ 2;

    //p3DCenario->Seleciona(point.x(),point.y());

    //mCursor.setPos(centro);
    //oldPosX = centro.x();
    //oldPosY = centro.y();

    oldPosX = point.x();
    oldPosY = point.y();

    mouseBtnPress = true;

}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    QPoint point,centro;
    centro.rx() = width ()>>1;
    centro.ry() = height()>>1;
    point= event->pos();
    actPosX = (float)(point.y() - oldPosY ) / 3.0f;
    actPosY = (float)(point.x() - oldPosX ) / 3.0f;
    if(japosicionou)
    {
       if(mouseBtnPress)
       {
          pCam->MapeiaMouse(actPosX,actPosY);
       }
    }
    oldPosX= point.x();
    oldPosY= point.y();
}

void GLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    QPoint point;

    point= event->pos();

    oldPosX= point.x();
    oldPosY= point.y();

    mouseBtnPress = false;
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
   switch(event->key())
   {
       case Qt::Key_A :{pCam->CameraStrafe(-150.0f);}break;
       case Qt::Key_B :{}break;
       case Qt::Key_C :{}break;
       case Qt::Key_D :{pCam->CameraStrafe(150.0f);}break;
       case Qt::Key_E :{}break;
       case Qt::Key_F :{}break;
       case Qt::Key_G :{}break;
       case Qt::Key_H :{}break;
       case Qt::Key_I :{}break;
       case Qt::Key_J :{}break;
       case Qt::Key_K :{}break;
       case Qt::Key_L :{}break;
       case Qt::Key_M :{}break;
       case Qt::Key_N :{}break;
       case Qt::Key_O :{}break;
       case Qt::Key_P :{}break;
       case Qt::Key_Q :{}break;
       case Qt::Key_R :{}break;
       case Qt::Key_S :{pCam->MoverCamera(-150.0f);}break;
       case Qt::Key_T :{}break;
       case Qt::Key_U :{}break;
       case Qt::Key_V :{}break;
       case Qt::Key_W :{pCam->MoverCamera(150.0f);}break;
       case Qt::Key_X :{}break;
       case Qt::Key_Y :{}break;
       case Qt::Key_Z :{}break;
       case Qt::Key_Escape:{}break;
   }
}

