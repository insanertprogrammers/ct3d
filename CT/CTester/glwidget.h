#ifndef GLWIDGET_H
#define GLWIDGET_H
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QMouseEvent>
#include "../INS3D/INS3D.h"
#include "../INSUT/INSUT.h"
#include "../INSIA/INSIA.h"

class CTSceneObject
{
public:
    CTSceneObject(){}

    virtual void Render   (                                         ){}
    virtual void Translate(CIMTVetor pTransf,bool      Incremento   ){}
    virtual void Rotate   (CIMTVetor pTransf,CIMTVetor pControlPoint){}
    virtual void Scale    (CIMTVetor pTransf,CIMTVetor pControl     ){}

private:
    int CT_ObjectType;
};

class CTSkyBox : public CTSceneObject
{
public:
     CTSkyBox()
     {
         int IdMatSky;

         /*************************/
         /*Cria Uma SkyBox        */
         /*************************/
         pSky     = new CI3DCubo  (CIMTVetor(0.0f,0.0f,0.0f),
                                   CIMTVetor(4096.0f,4096.0f,4096.0f),
                                   IGL_WINMODE_CW);


         IdMatSky=INSMA::pINSMAMaterialManager->AddMaterial("Material SkyBox DemoProject",IGL_MATERIAL_CUBEMAP);

         INSMA::pINSMAMaterialManager->AddCubeMap(IdMatSky,
                                                  "world/Sky-box/WhiteDwarf/wd-right.png",
                                                  "world/Sky-box/WhiteDwarf/wd-left.png",
                                                  "world/Sky-box/WhiteDwarf/wd-top.png",
                                                  "world/Sky-box/WhiteDwarf/wd-bottom.png",
                                                  "world/Sky-box/WhiteDwarf/wd-back.png",
                                                  "world/Sky-box/WhiteDwarf/wd-front.png" );


         pSky->I3D_Mesh->SetaMaterial(IdMatSky);

         pSky->I3D_Mesh->SetaNormalFace(0,CIMTVetor( 0.0f,-1.0f, 0.0f));
         pSky->I3D_Mesh->SetaNormalFace(1,CIMTVetor( 1.0f, 0.0f, 0.0f));
         pSky->I3D_Mesh->SetaNormalFace(2,CIMTVetor( 0.0f, 0.0f,-1.0f));
         pSky->I3D_Mesh->SetaNormalFace(3,CIMTVetor( 0.0f, 0.0f, 1.0f));
         pSky->I3D_Mesh->SetaNormalFace(4,CIMTVetor(-1.0f, 0.0f, 0.0f));
         pSky->I3D_Mesh->SetaNormalFace(5,CIMTVetor( 0.0f, 1.0f, 0.0f));

         pSky->I3D_Mesh->SetaGeometryMode(IGL_GEOMETRY_DINAMIC);
     }
    ~CTSkyBox(){}

     void Render()
     {
         if(pSky)
         {
            pSky->Render(IGL_MODE_RENDER        ,
                         IGL_RENDER_TRIANGLES   ,
                         IGL_SHADING_MODE_NOONE);
         }
     }
     void Translate(CIMTVetor pTransf, bool Incremento)
     {
         pSky->Translate(pTransf,Incremento);
     }
     void Rotate(CIMTVetor pTransf,CIMTVetor pControlPoint)
     {
         pSky->Rotate(true,pTransf,pControlPoint);
     }
private:
     CI3DCubo  *pSky;
};

class CTTerrain : public CTSceneObject
{
public:
     CTTerrain()
     {
         iTam  =256;
         iQuad =1;
         fScale=0.5;

         pTerreno = new CI3DPainelHeightMap();

         //Adiciona o HeightMap para a Construção do Terreno(Não é utilizado como textura)
         //pTerreno->AddHeightMap("world/Terrenos/Piso.bmp",iQuad,fScale);
         pTerreno->AddHeightMap(iTam,iQuad,fScale);

         //Cria Os Materiais do Terreno.
         int IdMaterial;
         int IdTexturaBase;

         //Adiciona o material e texturas adicionais do terreno(Utilizados para renderização normal)
         IdMaterial = INSMA::pINSMAMaterialManager->AddMaterial("Material Terreno Construtor",IGL_MATERIAL_MIXED);

         IdTexturaBase = INSMA::pINSMAMaterialManager->AddTextura(IdMaterial,
                                                                  "world/Terrenos/ColorMap.bmp",
                                                                  IGL_TEXTURE_GEN_ST);

         //Adiciona Layer Detail na Textura Base(Id 0)
         INSMA::pINSMAMaterialManager->AddLayerTex(IdMaterial                     ,
                                                   IdTexturaBase                  ,
                                                   "world/Texturas/Piso_Areia.bmp",
                                                   IMA_LAYER_CHN_DETAIL          );

         pTerreno->I3D_Mesh->SetaMaterial(IdMaterial);
     }

    ~CTTerrain(){}

     void Render()
     {
         if(pTerreno)
         {
            pTerreno->Render(IGL_MODE_RENDER            ,
                             IGL_RENDER_TRIANGLE_STRIP  ,
                             IGL_SHADING_MODE_FLAT);
         }
     }
     CIMTVetor PegaPosTrr(int x, int y)
     {
         CIMTVetor pos;
         pos.x =x * iQuad;
         pos.y = pTerreno->PegaAltura(x,y) * fScale;
         pos.z =y * iQuad;
         return pos;
     }
     CIMTVetor PegaCentro()
     {
         CIMTVetor pos;
         pos.x = (iTam/2) * iQuad ;
         pos.y = pTerreno->PegaAltura(iTam/2,iTam/2) * fScale;
         pos.z = (iTam/2) * iQuad;
         return  pos;
     }
     int PegaTam(int &tQuad,float &tScale)
     {
         tQuad = iQuad;
         tScale= fScale;

         return iTam;
     }
private:
     CI3DPainelHeightMap  *pTerreno;
     int   iTam;
     int   iQuad;
     float fScale;
};

class CTStaticModel : public CTSceneObject
{
public:

    CTStaticModel()
    {
        /*****************************************************************/
        /*Cria Modelo Detalhado(Muitos Vertices                          */
        /*****************************************************************/
        pModelo  = new CI3DModelo("world/Modelos/Ogro.obj","Material Ogro",false);

        //Modo de geometria dinâmico permite movimentação
        pModelo->I3D_Mesh->SetaGeometryMode(IGL_GEOMETRY_DINAMIC);
    }
    void Render()
    {
        if(pModelo)
        {
            /*************************************************************************************************/
            /*Renderiza o modelo de acordo com os parametros de iluminação e materiais                       */
            /*IGL_MATERIAL_COLOR   -> Renderiza Material + Iluminação                                        */
            /*IGL_MATERIAL_TEXTURE -> Renderiza Material + Textura + Iluminação                              */
            /*IGL_MATERIAL_MIXED   -> Renderiza Material + Textura + Iluminação(Mixa Textura com iluminação) */
            /*                                                                                               */
            /*IGL_SHADING_MODE_FLAT  -> Realiza o Shading Phong  Flat  (Normais Planares)                    */
            /*IGL_SHADING_MODE_SMOOTH-> Realiza o Shading Phong  Smooth(Normais partindo do centro)          */
            /*IGL_SHADING_MODE_DIFLAT-> Realiza o Shading Difuso Flat                                        */
            /*IGL_SHADING_MODE_DIFOTH-> Realiza o Shading Difuso Smooth                                      */
            /*                                                                                               */
            /*************************************************************************************************/
            pModelo->Render(IGL_MODE_RENDER     ,
                            IGL_RENDER_TRIANGLES,
                            IGL_SHADING_MODE_SMOOTH);
        }
    }
    void Translate(CIMTVetor pTransf, bool Incremento)
    {
        pModelo->Translate(pTransf,Incremento);
    }
    void Rotate(CIMTVetor pTransf,CIMTVetor pControlPoint)
    {
        pModelo->Rotate(true,pTransf,pControlPoint);
    }
private:
    CI3DModelo  *pModelo;//Modelo Ogro
};

class CTFlagModel : public CTSceneObject
{
public:

    CTFlagModel()
    {
        /*****************************************************************/
        /*Cria Modelo Detalhado(Muitos Vertices                          */
        /*****************************************************************/
        pModelo  = new CI3DModelo("world/Modelos/Torre.obj","Material Torre",false);

        //Modo de geometria dinâmico permite movimentação
        pModelo->I3D_Mesh->SetaGeometryMode(IGL_GEOMETRY_DINAMIC);
    }
    void Render()
    {
        if(pModelo)
        {
            /*************************************************************************************************/
            /*Renderiza o modelo de acordo com os parametros de iluminação e materiais                       */
            /*IGL_MATERIAL_COLOR   -> Renderiza Material + Iluminação                                        */
            /*IGL_MATERIAL_TEXTURE -> Renderiza Material + Textura + Iluminação                              */
            /*IGL_MATERIAL_MIXED   -> Renderiza Material + Textura + Iluminação(Mixa Textura com iluminação) */
            /*                                                                                               */
            /*IGL_SHADING_MODE_FLAT  -> Realiza o Shading Phong  Flat  (Normais Planares)                    */
            /*IGL_SHADING_MODE_SMOOTH-> Realiza o Shading Phong  Smooth(Normais partindo do centro)          */
            /*IGL_SHADING_MODE_DIFLAT-> Realiza o Shading Difuso Flat                                        */
            /*IGL_SHADING_MODE_DIFOTH-> Realiza o Shading Difuso Smooth                                      */
            /*                                                                                               */
            /*************************************************************************************************/
            pModelo->Render(IGL_MODE_RENDER     ,
                            IGL_RENDER_TRIANGLES,
                            IGL_SHADING_MODE_SMOOTH);
        }
    }
    void Translate(CIMTVetor pTransf, bool Incremento)
    {
        pModelo->Translate(pTransf,Incremento);
    }
    void Rotate(CIMTVetor pTransf,CIMTVetor pControlPoint)
    {
        pModelo->Rotate(true,pTransf,pControlPoint);
    }
    void Scale(CIMTVetor  pTransf,CIMTVetor  pControl  )
    {
        pModelo->Scale(false,pTransf,pControl);
    }
    CIMTVetor Pos()
    {
        return pModelo->I3D_RefPos;
    }
private:
    CI3DModelo  *pModelo;//Modelo Torre
};

class CTHierarquicalModel : public CTSceneObject
{
public:

    CTHierarquicalModel()
    {
        /*****************************************************************/
        /*Cria Modelo Detalhado(Muitos Vertices                          */
        /*****************************************************************/
        pModelo  = new CI3DModeloHierarquico("world/Modelos/MarauderCarBlender.obj",false);
        //pModelo  = new CI3DModeloHierarquico("world/Modelos/car.obj",false);
        //pModelo  = new CI3DModeloHierarquico("world/Modelos/caveirao.fbx",false);
        //pModelo  = new CI3DModeloHierarquico("world/Modelos/tanque.obj",false);

        status=0;
    }
    void Render()
    {
        if(pModelo)
        {
            /*************************************************************************************************/
            /*Renderiza o modelo de acordo com os parametros de iluminação e materiais                       */
            /*IGL_MATERIAL_COLOR   -> Renderiza Material + Iluminação                                        */
            /*IGL_MATERIAL_TEXTURE -> Renderiza Material + Textura + Iluminação                              */
            /*IGL_MATERIAL_MIXED   -> Renderiza Material + Textura + Iluminação(Mixa Textura com iluminação) */
            /*                                                                                               */
            /*IGL_SHADING_MODE_FLAT  -> Realiza o Shading Phong  Flat  (Normais Planares)                    */
            /*IGL_SHADING_MODE_SMOOTH-> Realiza o Shading Phong  Smooth(Normais partindo do centro)          */
            /*IGL_SHADING_MODE_DIFLAT-> Realiza o Shading Difuso Flat                                        */
            /*IGL_SHADING_MODE_DIFOTH-> Realiza o Shading Difuso Smooth                                      */
            /*                                                                                               */
            /*************************************************************************************************/
            pModelo->Render(IGL_MODE_RENDER     ,
                            IGL_RENDER_TRIANGLES,
                            IGL_SHADING_MODE_SMOOTH);
        }
    }
    void Translate(CIMTVetor pTransf, bool Incremento)
    {
        pModelo->Translate(pTransf,Incremento);
    }
    void Rotate(CIMTVetor pTransf,CIMTVetor pControlPoint)
    {
        pModelo->Rotate(true,pTransf,pControlPoint);
    }
    void Scale(CIMTVetor  pTransf,CIMTVetor  pControl  )
    {
        pModelo->Scale(true,pTransf,pControl);
    }
    int GetStatus()
    {
        return status;
    }
    void SetStatus(int pStatus)
    {
        status = pStatus;
    }
    void GetPos(CIMTVetor &pPos)
    {
        pPos = pModelo->I3D_RefPos;
    }

    int                     status; //0-idle 1-andando

private:

    CI3DModeloHierarquico  *pModelo;//Modelo Ogro

};

class CTLight : public CTSceneObject
{
public:
    CTLight()
    {
        /***********************************************/
        /*Cria as Luzes utilizadas no cenario          */
        /***********************************************/
        pLuzAmbiente = new CI3DLuz();
        pLuzAmbiente->SetaCorDifusa  (CIMACor(0.0f ,0.07f,0.0f ,1.0f));
        pLuzAmbiente->SetaCorSpecular(CIMACor(0.7f ,0.0f ,0.0f ,1.0f));
        pLuzAmbiente->SetaCorAmbiente(CIMACor(0.0f ,0.0f ,0.2f ,1.0f));
        pLuzAmbiente->SetaAtenuacao(0.01f,0.001f,0.0001f);
        pLuzAmbiente->Prepara(INSSH::pISHProgramManager->ProgramId("CoreRender"),0);
    }
    void Translate(CIMTVetor pTransf, bool Incremento)
    {
        pLuzAmbiente->SetaPos(pTransf);
    }
    void Animate(CIMTVetor pCenter)
    {
        CIMTMatriz mtxTransf;
        CIMTVetor  dest;
        /************************************/
        /*Anima a Luz, como uma luz dinamica*/
        /************************************/
        mtxTransf.RotateY(0.01f,pCenter,true);
        dest = pLuzAmbiente->PegaPos() * mtxTransf;
        pLuzAmbiente->SetaPos(dest);
        pLuzAmbiente->Atualiza(INSSH::pISHProgramManager->ProgramId("CoreRender"),IGL_LIGHT_CHANNEL_01);
    }
private:
    CI3DLuz  *pLuzAmbiente  ;//Luz Ambiente
};

class CTPathfind : public CTSceneObject
{
public:
    CTPathfind()
    {


    }
    void SetTerrain(CTTerrain *pTerrain)
    {
        int iTam;
        int iQuad;
        float fScale;

        PathTerrain = pTerrain;

        iTam=PathTerrain->PegaTam(iQuad,fScale);

        pMapa       = new CIIAMapa   (iTam );
        pPathFinder = new CIIAestrela(pMapa);

        pPathStep = 0;
    }
    void SetIni(float pIniX,float pIniY)
    {
        iIniX = pIniX;
        iIniY = pIniY;
    }
    void SetFim(float pFimX,float pFimY)
    {
        iFimX = pFimX;
        iFimY = pFimY;
    }
    void FindPath()
    {
        pPathFinder->reset();
        pPathFinder->grid->SetaIni(iIniX,iIniY);
        pPathFinder->grid->SetaFim(iFimX,iFimY);
        pStatusBusca = PROCURANDO;
        while(pStatusBusca==PROCURANDO)
        {
            pStatusBusca = pPathFinder->Passo();
        }

        GetPath(pPath);
    }

    void GetPath(vector<CIMTVetor> &pPath)
    {
        CIIANoEstrela* pPathRoot;

        if(pStatusBusca!=PROCURANDO)
        {
            switch(pStatusBusca)
            {
               case IMPOSSIVEL:{return;}
               case ACHOUCAMINHO:
               {
                   pPathRoot = pPathFinder->PegaCaminho();
                   while(pPathRoot!=NULL)
                   {
                       pPath.push_back(PathTerrain->PegaPosTrr(pPathRoot->PosNoX,pPathRoot->PosNoY));
                       pPathRoot = (CIIANoEstrela*)pPathRoot->n_pai;
                   }
               }
            }
            std::reverse(pPath.begin(), pPath.end());
            pPathStep=0;
        }
    }
    bool GetNextStep(CIMTVetor &pStep)
    {
        if(pPathStep < pPath.size())
        {
            pStep = pPath[pPathStep];
            pPathStep++;
        }
        else
        {
            pPathStep=0;
            return false;
        }
    }

private:
    CIIAMapa    *pMapa;
    CIIAestrela *pPathFinder;
    CTTerrain   *PathTerrain;

    vector<CIMTVetor> pPath;
    int               pPathStep;

    STDANDAMENTO pStatusBusca;
    int iIniX,iIniY;
    int iFimX,iFimY;
};


class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
      GLWidget(QWidget *parent = nullptr);
     ~GLWidget()
      {
          cleanup();
      }

      void keyPressEvent(QKeyEvent *event) override;

public slots:
    void cleanup();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void UpdateScene();
    void SetInitPosition(CI3DCamera *pCam, CIMTVetor centro);

private:
    float         TimeElapsed;
    float         oldPosX,
                  oldPosY;
    float         actPosX,
                  actPosY;
    bool          mouseBtnPress;
    QTimer        *timer;
    QElapsedTimer *elapsedtimer;

    CI3DCamera    *pCam                 ;//Camera principal de visualização
    CI3DCubo      *pCubo                ;//Cubo a ser desenhado
    bool           japosicionou         ;//Flag informando se ja posicionou a camera

    INS3D  *pI3DComponent; //Componente 3D

    //Objetos de Teste de Componentes
    //CTSkyBox            *pSkyBox;
    //CTTerrain           *pTerrain;
    //CTStaticModel       *pModel;
    //CTFlagModel         *pTorre;
    //CTHierarquicalModel *pCharacter;
    //CTLight             *pLight;
    //CTPathfind          *pLocator;

    CTSceneObject         *pSkyBox;
    CTSceneObject         *pTerrain;
    CTSceneObject         *pModel;
    CTSceneObject         *pTorre;
    CTSceneObject         *pCharacter;
    CTSceneObject         *pLight;
    CTSceneObject         *pLocator;

    //Objetos Genéricos de Teste
    vector<CTSceneObject*>   pObjectPool;
    vector<CIUTGenericData*> pGenericPool;

};


#endif // GLWIDGET_H
