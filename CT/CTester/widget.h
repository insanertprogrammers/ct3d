#ifndef WIDGET_H
#define WIDGET_H

#include <QApplication>
#include <QDesktopWidget>
#include <QWidget>
#include <glwidget.h>

class Widget : public QWidget
{
    Q_OBJECT

public:
     Widget(QWidget *parent = nullptr);
    ~Widget();

protected:
     void keyPressEvent(QKeyEvent    *event) override;
     void resizeEvent  (QResizeEvent *event) override;


private:
     //Widget de Renderização
     GLWidget *pGLWidget;
};
#endif // WIDGET_H
