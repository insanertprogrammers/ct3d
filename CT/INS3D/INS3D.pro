#-------------------------------------------------
#
# Project created by QtCreator 2014-04-18T18:43:03
#
#-------------------------------------------------

QT       += gui opengl openglextensions multimedia

TARGET = INS3D
TEMPLATE = lib
CONFIG += staticlib


SOURCES += INS3D.cpp \
    I3D_Camera.cpp \
    I3D_Cilindro.cpp \
    I3D_Cubo.cpp \
    I3D_Esfera.cpp \
    I3D_Face.cpp \
    I3D_Luz.cpp \
    I3D_Mesh.cpp \
    I3D_Modelo.cpp \
    I3D_Painel.cpp \
    I3D_Poligono.cpp \
    I3D_Primitivo.cpp \
    I3D_Vertice.cpp \
    I3D_Texto.cpp \
    I3D_PainelHeightMap.cpp \
    I3D_Fonte.cpp \
    I3D_ModeloHierarquico.cpp \
    I3D_ArvoreBsp.cpp \
    I3D_FaceBsp.cpp \
    I3D_FolhaBsp.cpp \
    I3D_NoBsp.cpp \
    I3D_ModeloLowPoly.cpp \
    I3D_ProcessadorHSR.cpp \
    I3D_ProcessadorCSG.cpp \
    I3D_ProcessadorRON.cpp \
    I3D_Portal.cpp \
    I3D_ProcessadorPRT.cpp \
    I3D_ProcessadorPVS.cpp \
    I3D_LightMap.cpp

HEADERS += INS3D.h \
    I3D_Camera.h \
    I3D_Cilindro.h \
    I3D_Cubo.h \
    I3D_Esfera.h \
    I3D_Face.h \
    I3D_GlobalDef.h \
    I3D_Luz.h \
    I3D_Mesh.h \
    I3D_Modelo.h \
    I3D_Painel.h \
    I3D_Poligono.h \
    I3D_Primitivo.h \
    I3D_Vertice.h \
    I3D_Texto.h \
    I3D_PainelHeightMap.h \
    I3D_Fonte.h \
    I3D_ModeloHierarquico.h \
    I3D_ArvoreBsp.h \
    I3D_FaceBsp.h \
    I3D_FolhaBsp.h \
    I3D_NoBsp.h \
    I3D_ModeloLowPoly.h \
    I3D_ProcessadorHSR.h \
    I3D_ProcessadorCSG.h \
    I3D_ProcessadorRON.h \
    I3D_Portal.h \
    I3D_ProcessadorPRT.h \
    I3D_ProcessadorPVS.h \
    I3D_LightMap.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

#INSGL
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSGL/release/ -lINSGL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSGL/debug/ -lINSGL
else:unix: LIBS += -L$$OUT_PWD/../INSGL/ -lINSGL

INCLUDEPATH += $$PWD/../INSGL
DEPENDPATH += $$PWD/../INSGL

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/release/libINSGL.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/debug/libINSGL.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/release/INSGL.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSGL/debug/INSGL.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../INSGL/libINSGL.a

#INSMT
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSMT/release/ -lINSMT
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSMT/debug/ -lINSMT
else:unix: LIBS += -L$$OUT_PWD/../INSMT/ -lINSMT

INCLUDEPATH += $$PWD/../INSMT
DEPENDPATH += $$PWD/../INSMT

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/release/libINSMT.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/debug/libINSMT.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/release/INSMT.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMT/debug/INSMT.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../INSMT/libINSMT.a

#INSSH
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSSH/release/ -lINSSH
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSSH/debug/ -lINSSH
else:unix: LIBS += -L$$OUT_PWD/../INSSH/ -lINSSH

INCLUDEPATH += $$PWD/../INSSH
DEPENDPATH += $$PWD/../INSSH

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/release/libINSSH.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/debug/libINSSH.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/release/INSSH.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSSH/debug/INSSH.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../INSSH/libINSSH.a

#INSMA
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSMA/release/ -lINSMA
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSMA/debug/ -lINSMA
else:unix: LIBS += -L$$OUT_PWD/../INSMA/ -lINSMA

INCLUDEPATH += $$PWD/../INSMA
DEPENDPATH += $$PWD/../INSMA

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/release/libINSMA.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/debug/libINSMA.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/release/INSMA.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSMA/debug/INSMA.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../INSMA/libINSMA.a

#OpenCV
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_calib3d410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_core410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_dnn410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_features2d410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_flann410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_gapi410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_highgui410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_imgcodecs410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_imgproc410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_ml410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_objdetect410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_photo410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_stitching410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_video410.dll
win32: LIBS += -L$$PWD/../libs/opencv/MinGW/cv73064/x64/mingw/lib/ -llibopencv_videoio410.dll

INCLUDEPATH += $$PWD/../libs/opencv/MinGW/cv73064/include
DEPENDPATH += $$PWD/../libs/opencv/MinGW/cv73064/include

#INSUT
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../INSUT/release/ -lINSUT
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../INSUT/debug/ -lINSUT

INCLUDEPATH += $$PWD/../INSUT
DEPENDPATH += $$PWD/../INSUT

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/release/libINSUT.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/debug/libINSUT.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/release/INSUT.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../INSUT/debug/INSUT.lib

#assimp
win32: LIBS += -L$$PWD/../libs/assimp/MinGW/lib/ -llibassimp.dll

INCLUDEPATH += $$PWD/../libs/assimp/MinGW/include
DEPENDPATH += $$PWD/../libs/assimp/MinGW/include
